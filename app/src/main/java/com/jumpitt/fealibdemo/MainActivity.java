package com.jumpitt.fealibdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.jumpitt.fea_library.FEAClient;
import com.jumpitt.fea_library.FEAListener;
import com.jumpitt.fea_library.model.Auth;

public class MainActivity extends AppCompatActivity {

    private FEAClient mClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FEAClient.init(this);

        mClient = FEAClient.getInstance();

        authenticateExample();

    }

    private void authenticateExample() {
        mClient.authenticate("18584e49-6778-46c5-a1d2-47bbf89c807f", new FEAListener<Auth>() {
            @Override
            public void onSuccess(Auth auth) {
                Log.d("TAG", auth.getToken());

                mClient.getPendingRequests("17836815-K", new FEAListener<Boolean>() {
                    @Override
                    public void onSuccess(Boolean data) {
                        Log.d("TAG", data ? "TIENE" : "NO TIENE");
                    }

                    @Override
                    public void onError(Throwable t) {
                        Log.e("TAG", "NO", t);
                    }
                });
            }

            @Override
            public void onError(Throwable throwable) {
                Log.e("TAG", "FAIL", throwable);
            }
        });
    }

    /*private void creationExample() {
        mClient.createCertificate("1234214123", "aCEPTA1234", "Asdf1234", new FEAListener<Certificate>() {
            @Override
            public void onSuccess(Certificate certificate) {
                Toast.makeText(MainActivity.this, certificate.getName(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(Throwable throwable) {
                throwable.printStackTrace();
                Toast.makeText(MainActivity.this, throwable.getCause().getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }*/
}
