package com.jumpitt.fea_library;

import com.jumpitt.fea_library.model.Auth;
import com.jumpitt.fea_library.model.BaseResponse;
import com.jumpitt.fea_library.model.Certificate;
import com.jumpitt.fea_library.model.CertificateStatus;
import com.jumpitt.fea_library.model.SignResponse;
import com.jumpitt.fea_library.model.TransactionList;

interface FEAContract {
    void authenticate(String mobileID, FEAListener<Auth> responseListener);

    void createCertificate(String requestNumber, String pin, String password, String phone, FEAListener<Certificate> responseListener);

    void getTransactionList(String status, FEAListener<TransactionList> responseListener);

    void getTransaction(String status, String transactionID, FEAListener<TransactionList> responseListener);

    void signTransaction(String transactionID, String password, FEAListener<SignResponse> responseListener);

    void rejectTransaction(String transactionID, String password, FEAListener<SignResponse> responseListener);

    void signTransaction(String transactionID, String webSocketId, String password, FEAListener<SignResponse> responseListener);

    void rejectTransaction(String transactionID, String webSocketId, String password, FEAListener<SignResponse> responseListener);

    void signTransactionDEC(String documentCode, String dni, String password, FEAListener<SignResponse> responseListener);

    void rejectTransactionDEC(String documentCode, String dni, String password, FEAListener<SignResponse> responseListener);

    void getCertificateStatus(FEAListener<CertificateStatus> listener);

    void getPendingRequests(String dni, FEAListener<Boolean> responseListener);
}
