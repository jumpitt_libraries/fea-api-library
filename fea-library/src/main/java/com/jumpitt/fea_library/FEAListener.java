package com.jumpitt.fea_library;

public interface FEAListener<T> {
    void onSuccess(T data);

    void onError(Throwable t);
}
