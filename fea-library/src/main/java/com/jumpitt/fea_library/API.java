package com.jumpitt.fea_library;

import com.jumpitt.fea_library.model.Auth;
import com.jumpitt.fea_library.model.Certificate;
import com.jumpitt.fea_library.model.CertificateStatus;
import com.jumpitt.fea_library.model.RequestResponse;
import com.jumpitt.fea_library.model.SignResponse;
import com.jumpitt.fea_library.model.TransactionList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface API {
    @Headers("API-KEY:1dZGyoH3GvlbJNoFVxKcd5uFjmFd2jNkL4Ta+okx4X0=")
    @POST("auth")
    Call<Auth> auth(@Body AuthBody body);

    @POST("certificate")
    Call<Certificate> certificate(@Header("token") String token, @Body CertificateBody body);


    @POST("sign/transaction")
    Call<TransactionList> getTransactions(@Header("token") String header, @Body TransactionBody body);


    @POST("sign")
    Call<SignResponse> sign(@Header("token") String header, @Body SignBody body);

    @POST("sign/requestAndSign")
    Call<SignResponse> signDEC(@Header("token") String header, @Body SignDECBody body);

    @GET("certificate/status")
    Call<CertificateStatus> status(@Header("token") String header);

    @POST("certificate/solicitudes")
    Call<RequestResponse> requests(@Header("token") String header, @Body StatusBody body);

}
