package com.jumpitt.fea_library;

import com.google.gson.annotations.SerializedName;

class CertificateBody {
    @SerializedName("nroSolicitud")
    private String requestNumber;
    @SerializedName("pinEncriptado")
    private String encryptPin;
    @SerializedName("passwordFirma")
    private String signPassword;
    @SerializedName("numeroTelefono")
    private String phone;

    public CertificateBody(String publicKey, String requestNumber, String pin, String password, String phone) {
        String encryptedPin = Utils.encrypt(publicKey, pin);
        String encryptedPassword = Utils.encrypt(publicKey, password);
        this.requestNumber = requestNumber;
        this.encryptPin = encryptedPin;
        this.signPassword = encryptedPassword;
        this.phone = phone;
    }
}
