package com.jumpitt.fea_library;

import android.content.Context;
import android.content.SharedPreferences;

import com.jumpitt.fea_library.model.Auth;
import com.jumpitt.fea_library.model.Certificate;
import com.jumpitt.fea_library.model.CertificateStatus;
import com.jumpitt.fea_library.model.SignResponse;
import com.jumpitt.fea_library.model.TransactionList;

public class FEAClient implements FEAContract {
    public static final String PENDING_STATUS = "PENDIENTE";
    public static final String SIGNED_STATUS = "FIRMADO";
    public static final String REJECTED_STATUS = "RECHAZADO";
    private static final String PREFERENCES_NAME = "FEA_CLIENT_PREFERENCES";
    private static final String MOBILE_ID = "MOBILE_ID";
    private static final String TOKEN = "TOKEN";
    private static final String PUB_KEY = "PUB_KEY";
    private static final String FEA_VALUE = "FEA_VALUE";
    private static final int PREFERENCES_MODE = Context.MODE_PRIVATE;
    private static FEAClient feaClient;
    private static SharedPreferences sharedPreferences;

    /**
     * Initialize a new Client Singleton
     * <p>
     * Call this into you Application class
     */
    public static void init(Context context) {
        feaClient = new FEAClient();
        sharedPreferences = context.getSharedPreferences(PREFERENCES_NAME, PREFERENCES_MODE);
    }

    /**
     * Obtain a valid reference of FEA client
     *
     * @return FEAClient instance
     */
    public static FEAClient getInstance() {
        return feaClient;
    }

    public static boolean isActivated() {
        return sharedPreferences.contains(FEA_VALUE);
    }

    public static void clearData() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(FEA_VALUE);
        editor.apply();
    }

    public static boolean isValid(String password) {
        String passwordStored = sharedPreferences.getString(FEA_VALUE, null);
        return passwordStored != null && passwordStored.equals(password);
    }

    /**
     * Authenticate the phone to get a session token and a public key for encryption
     *
     * @param mobileID Device identifier (must be the Trust ID)
     */
    @Override
    public void authenticate(final String mobileID, final FEAListener<Auth> responseListener) {
        Queries.doAuth(mobileID, new FEAListener<Auth>() {
            @Override
            public void onSuccess(Auth data) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(MOBILE_ID, mobileID);
                editor.putString(TOKEN, data.getToken());
                editor.putString(PUB_KEY, data.getPubKey());
                editor.apply();
                responseListener.onSuccess(data);
            }

            @Override
            public void onError(Throwable t) {
                responseListener.onError(t);
            }
        });
    }


    /**
     * Create a new certificate using the request number given
     *
     * @param requestNumber    number sent to the user email
     * @param pin              activation PIN created in Acepta OnBoard (For testing aCEPTA1234)
     * @param password         FEA password. It must contain at least 8 characters, 1 number and 1 uppercase letter
     * @param phone            Phone number registered
     * @param responseListener Callback to obtain the call response
     */
    @Override
    public void createCertificate(final String requestNumber, final String pin, final String password, final String phone, final FEAListener<Certificate> responseListener) {
        String token = sharedPreferences.getString(TOKEN, null);
        String publicKey = sharedPreferences.getString(PUB_KEY, "");
        Queries.doCreation(token, new CertificateBody(publicKey, requestNumber, pin, password, phone), new FEAListener<Certificate>() {
            @Override
            public void onSuccess(Certificate data) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(FEA_VALUE, password);
                editor.apply();
                responseListener.onSuccess(data);
            }

            @Override
            public void onError(Throwable t) {
                if (t.getMessage().equals("2")) {
                    String mobileId = sharedPreferences.getString(MOBILE_ID, null);
                    if (mobileId != null) {
                        authenticate(mobileId, new FEAListener<Auth>() {
                            @Override
                            public void onSuccess(Auth data) {
                                createCertificate(requestNumber, pin, password, phone, responseListener);
                            }

                            @Override
                            public void onError(Throwable t) {
                                responseListener.onError(t);
                            }
                        });
                    }
                } else {
                    responseListener.onError(t);
                }
            }
        });
    }

    /**
     * Get all pending transactions
     *
     * @param status           status of the document
     * @param responseListener Callback to obtain the call response
     */
    @Override
    public void getTransactionList(final String status, final FEAListener<TransactionList> responseListener) {
        String token = sharedPreferences.getString(TOKEN, null);

        Queries.getTransactions(status, token, new FEAListener<TransactionList>() {
            @Override
            public void onSuccess(TransactionList data) {
                responseListener.onSuccess(data);
            }

            @Override
            public void onError(Throwable t) {
                if (t.getMessage().equals("2")) {
                    String mobileId = sharedPreferences.getString(MOBILE_ID, null);
                    if (mobileId != null) {
                        authenticate(mobileId, new FEAListener<Auth>() {
                            @Override
                            public void onSuccess(Auth data) {
                                getTransactionList(status, responseListener);
                            }

                            @Override
                            public void onError(Throwable t) {
                                responseListener.onError(t);
                            }
                        });
                    }
                } else {
                    responseListener.onError(t);
                }
            }
        });
    }

    /**
     * Get detail of a single transaction
     *
     * @param status           status of the document
     * @param transactionID    the id of the transaction sought out
     * @param responseListener Callback to obtain the call response
     */
    @Override
    public void getTransaction(final String status, final String transactionID, final FEAListener<TransactionList> responseListener) {
        String token = sharedPreferences.getString(TOKEN, null);

        Queries.getTransaction(status, token, transactionID, new FEAListener<TransactionList>() {
            @Override
            public void onSuccess(TransactionList data) {
                responseListener.onSuccess(data);
            }

            @Override
            public void onError(Throwable t) {
                if (t.getMessage().equals("2")) {
                    String mobileId = sharedPreferences.getString(MOBILE_ID, null);
                    if (mobileId != null) {
                        authenticate(mobileId, new FEAListener<Auth>() {
                            @Override
                            public void onSuccess(Auth data) {
                                getTransaction(status, transactionID, responseListener);
                            }

                            @Override
                            public void onError(Throwable t) {
                                responseListener.onError(t);
                            }
                        });
                    }
                } else {
                    responseListener.onError(t);
                }
            }
        });
    }

    /**
     * Sign a single transaction
     *
     * @param transactionID    the id of the transaction to sign
     * @param password         FEA password created on {@link #createCertificate(String, String, String, String, FEAListener)}
     * @param responseListener Callback to obtain the call response
     */
    @Override
    public void signTransaction(final String transactionID, final String password, final FEAListener<SignResponse> responseListener) {
        String token = sharedPreferences.getString(TOKEN, null);
        String publicKey = sharedPreferences.getString(PUB_KEY, "");

        Queries.signDocument(publicKey, token, transactionID, password, new FEAListener<SignResponse>() {
            @Override
            public void onSuccess(SignResponse data) {
                responseListener.onSuccess(data);
            }

            @Override
            public void onError(Throwable t) {
                if (t.getMessage().equals("2")) {
                    String mobileId = sharedPreferences.getString(MOBILE_ID, null);
                    if (mobileId != null) {
                        authenticate(mobileId, new FEAListener<Auth>() {
                            @Override
                            public void onSuccess(Auth data) {
                                signTransaction(transactionID, password, responseListener);
                            }

                            @Override
                            public void onError(Throwable t) {
                                responseListener.onError(t);
                            }
                        });
                    }
                } else {
                    responseListener.onError(t);
                }
            }
        });
    }

    /**
     * Reject a single transaction
     *
     * @param transactionID    the id of the transaction to sign
     * @param password         FEA password created on {@link #createCertificate(String, String, String, String, FEAListener)}
     * @param responseListener Callback to obtain the call response
     */
    @Override
    public void rejectTransaction(final String transactionID, final String password, final FEAListener<SignResponse> responseListener) {
        String token = sharedPreferences.getString(TOKEN, null);
        String publicKey = sharedPreferences.getString(PUB_KEY, "");

        Queries.rejectDocument(publicKey, token, transactionID, password, new FEAListener<SignResponse>() {
            @Override
            public void onSuccess(SignResponse data) {
                responseListener.onSuccess(data);
            }

            @Override
            public void onError(Throwable t) {
                if (t.getMessage().equals("2")) {
                    String mobileId = sharedPreferences.getString(MOBILE_ID, null);
                    if (mobileId != null) {
                        authenticate(mobileId, new FEAListener<Auth>() {
                            @Override
                            public void onSuccess(Auth data) {
                                rejectTransaction(transactionID, password, responseListener);
                            }

                            @Override
                            public void onError(Throwable t) {
                                responseListener.onError(t);
                            }
                        });
                    }
                } else {
                    responseListener.onError(t);
                }
            }
        });
    }

    /**
     * Sign a single transaction
     *
     * @param transactionID    the id of the transaction to sign
     * @param webSocketId      the id of the web socket
     * @param password         FEA password created on {@link #createCertificate(String, String, String, String, FEAListener)}
     * @param responseListener Callback to obtain the call response
     */
    @Override
    public void signTransaction(final String transactionID, final String webSocketId, final String password, final FEAListener<SignResponse> responseListener) {
        String token = sharedPreferences.getString(TOKEN, null);
        String publicKey = sharedPreferences.getString(PUB_KEY, "");

        Queries.signDocument(publicKey, webSocketId, token, transactionID, password, new FEAListener<SignResponse>() {
            @Override
            public void onSuccess(SignResponse data) {
                responseListener.onSuccess(data);
            }

            @Override
            public void onError(Throwable t) {
                if (t.getMessage().equals("2")) {
                    String mobileId = sharedPreferences.getString(MOBILE_ID, null);
                    if (mobileId != null) {
                        authenticate(mobileId, new FEAListener<Auth>() {
                            @Override
                            public void onSuccess(Auth data) {
                                signTransaction(transactionID, webSocketId, password, responseListener);
                            }

                            @Override
                            public void onError(Throwable t) {
                                responseListener.onError(t);
                            }
                        });
                    }
                } else {
                    responseListener.onError(t);
                }
            }
        });
    }

    /**
     * Reject a single transaction
     *
     * @param transactionID    the id of the transaction to reject
     * @param webSocketId      the id of the web socket
     * @param password         FEA password created on {@link #createCertificate(String, String, String, String, FEAListener)}
     * @param responseListener Callback to obtain the call response
     */
    @Override
    public void rejectTransaction(final String transactionID, final String webSocketId, final String password, final FEAListener<SignResponse> responseListener) {
        String token = sharedPreferences.getString(TOKEN, null);
        String publicKey = sharedPreferences.getString(PUB_KEY, "");

        Queries.rejectDocument(publicKey, webSocketId, token, transactionID, password, new FEAListener<SignResponse>() {
            @Override
            public void onSuccess(SignResponse data) {
                responseListener.onSuccess(data);
            }

            @Override
            public void onError(Throwable t) {
                if (t.getMessage().equals("2")) {
                    String mobileId = sharedPreferences.getString(MOBILE_ID, null);
                    if (mobileId != null) {
                        authenticate(mobileId, new FEAListener<Auth>() {
                            @Override
                            public void onSuccess(Auth data) {
                                rejectTransaction(transactionID, webSocketId, password, responseListener);
                            }

                            @Override
                            public void onError(Throwable t) {
                                responseListener.onError(t);
                            }
                        });
                    }
                } else {
                    responseListener.onError(t);
                }
            }
        });
    }

    @Override
    public void signTransactionDEC(final String documentCode, final String dni, final String password, final FEAListener<SignResponse> responseListener) {
        String token = sharedPreferences.getString(TOKEN, null);
        String publicKey = sharedPreferences.getString(PUB_KEY, "");

        Queries.signDEC(publicKey, token, dni, documentCode, password, "0", new FEAListener<SignResponse>() {
            @Override
            public void onSuccess(SignResponse data) {
                responseListener.onSuccess(data);
            }

            @Override
            public void onError(Throwable t) {
                if (t.getMessage().equals("2")) {
                    String mobileId = sharedPreferences.getString(MOBILE_ID, null);
                    if (mobileId != null) {
                        authenticate(mobileId, new FEAListener<Auth>() {
                            @Override
                            public void onSuccess(Auth data) {
                                signTransactionDEC(documentCode, dni, password, responseListener);
                            }

                            @Override
                            public void onError(Throwable t) {
                                responseListener.onError(t);
                            }
                        });
                    }
                } else {
                    responseListener.onError(t);
                }
            }
        });
    }

    @Override
    public void rejectTransactionDEC(final String documentCode, final String dni, final String password, final FEAListener<SignResponse> responseListener) {
        String token = sharedPreferences.getString(TOKEN, null);
        String publicKey = sharedPreferences.getString(PUB_KEY, "");
        Queries.signDEC(publicKey, token, dni, documentCode, password, "1", new FEAListener<SignResponse>() {
            @Override
            public void onSuccess(SignResponse data) {
                responseListener.onSuccess(data);
            }

            @Override
            public void onError(Throwable t) {
                if (t.getMessage().equals("2")) {
                    String mobileId = sharedPreferences.getString(MOBILE_ID, null);
                    if (mobileId != null) {
                        authenticate(mobileId, new FEAListener<Auth>() {
                            @Override
                            public void onSuccess(Auth data) {
                                signTransactionDEC(documentCode, dni, password, responseListener);
                            }

                            @Override
                            public void onError(Throwable t) {
                                responseListener.onError(t);
                            }
                        });
                    }
                } else {
                    responseListener.onError(t);
                }
            }
        });
    }

    /**
     * Get the certificate status (Rejected, expired, ...)
     *
     * @param responseListener Callback to obtain the call response
     */
    @Override
    public void getCertificateStatus(final FEAListener<CertificateStatus> responseListener) {
        String token = sharedPreferences.getString(TOKEN, null);

        Queries.status(token, new FEAListener<CertificateStatus>() {
            @Override
            public void onSuccess(CertificateStatus data) {
                responseListener.onSuccess(data);
            }

            @Override
            public void onError(Throwable t) {
                if (t.getMessage().equals("2")) {
                    String mobileId = sharedPreferences.getString(MOBILE_ID, null);
                    if (mobileId != null) {
                        authenticate(mobileId, new FEAListener<Auth>() {
                            @Override
                            public void onSuccess(Auth data) {
                                getCertificateStatus(responseListener);
                            }

                            @Override
                            public void onError(Throwable t) {
                                responseListener.onError(t);
                            }
                        });
                    }
                } else {
                    responseListener.onError(t);
                }
            }
        });
    }

    /**
     * Get the pending requests
     *
     * @param dni              User dni given on {@link #createCertificate(String, String, String, String, FEAListener)}
     * @param responseListener Callback to obtain the call response
     */
    @Override
    public void getPendingRequests(final String dni, final FEAListener<Boolean> responseListener) {
        String token = sharedPreferences.getString(TOKEN, null);

        Queries.getRequests(token, dni, new FEAListener<Boolean>() {
            @Override
            public void onSuccess(Boolean data) {
                responseListener.onSuccess(data);
            }

            @Override
            public void onError(Throwable t) {
                if (t.getMessage().equals("2")) {
                    String mobileId = sharedPreferences.getString(MOBILE_ID, null);
                    if (mobileId != null) {
                        authenticate(mobileId, new FEAListener<Auth>() {
                            @Override
                            public void onSuccess(Auth data) {
                                getPendingRequests(dni, responseListener);
                            }

                            @Override
                            public void onError(Throwable t) {
                                responseListener.onError(t);
                            }
                        });
                    }
                } else {
                    responseListener.onError(t);
                }
            }
        });
    }

}
