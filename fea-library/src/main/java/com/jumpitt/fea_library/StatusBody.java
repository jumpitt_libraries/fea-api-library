package com.jumpitt.fea_library;

import com.google.gson.annotations.SerializedName;

class StatusBody {
    @SerializedName("dniPersona")
    private String dni;

    public StatusBody(String dni) {
        this.dni = dni;
    }
}
