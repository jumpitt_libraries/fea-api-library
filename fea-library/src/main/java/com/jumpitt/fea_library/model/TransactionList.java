package com.jumpitt.fea_library.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TransactionList {
    @SerializedName("transacciones")
    private List<Transaction> transactions;

    public List<Transaction> getTransactions() {
        return transactions;
    }
}
