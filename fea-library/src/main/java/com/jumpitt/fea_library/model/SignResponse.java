package com.jumpitt.fea_library.model;

import com.google.gson.annotations.SerializedName;

public class SignResponse extends BaseResponse {
    @SerializedName("estadoTransaccion")
    private String result;

    public String getResult() {
        return result;
    }
}
