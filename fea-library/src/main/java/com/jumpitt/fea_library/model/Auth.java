package com.jumpitt.fea_library.model;

public class Auth extends BaseResponse {
    private String token;
    private String pubKey;
    private String modulus;
    private String exponent;

    public String getToken() {
        return token;
    }

    public String getPubKey() {
        return pubKey;
    }

    public String getModulus() {
        return modulus;
    }

    public String getExponent() {
        return exponent;
    }
}
