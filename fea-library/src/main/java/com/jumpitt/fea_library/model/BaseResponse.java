package com.jumpitt.fea_library.model;

public class BaseResponse {
    private int error;
    private String glosa;

    public int getError() {
        return error;
    }

    public String getGlosa() {
        return glosa;
    }
}
