package com.jumpitt.fea_library.model;

import com.google.gson.annotations.SerializedName;

public class Certificate extends BaseResponse {
    @SerializedName("nombre")
    private String name;
    @SerializedName("dniPersona")
    private String dni;

    public String getName() {
        return name;
    }

    public String getDni() {
        return dni;
    }
}
