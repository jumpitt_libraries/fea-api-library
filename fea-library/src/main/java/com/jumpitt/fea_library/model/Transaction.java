package com.jumpitt.fea_library.model;

import com.google.gson.annotations.SerializedName;

public class Transaction {
    @SerializedName("idTransaccion")
    private String id;
    @SerializedName("urlDocumento")
    private String url;
    @SerializedName("nombreDocumento")
    private String name;
    @SerializedName("estadoTransaccion")
    private String state;
    @SerializedName("fechaIngreso")
    private String createdAt;
    @SerializedName("fechaActualizacion")
    private String updatedAt;
    @SerializedName("institucion")
    private String institution;
    @SerializedName("logo")
    private String logo;

    public String getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getState() {
        return state;
    }

    public String getName() {
        return name;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getInstitution() {
        return institution;
    }

    public String getLogo() {
        return logo;
    }
}
