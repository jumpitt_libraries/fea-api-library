package com.jumpitt.fea_library.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RequestResponse extends BaseResponse {
    @SerializedName("resumenSolicitudData")
    private List<Request> requestList;

    public List<Request> getRequestList() {
        return requestList;
    }

    public class Request {
        @SerializedName("codigoSolicitud")
        private String request;
        @SerializedName("fecha")
        private String date;

        public String getRequest() {
            return request;
        }

        public String getDate() {
            return date;
        }
    }
}
