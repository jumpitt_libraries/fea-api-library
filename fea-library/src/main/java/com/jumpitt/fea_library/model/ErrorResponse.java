package com.jumpitt.fea_library.model;

public class ErrorResponse {
    private String error;
    private String glosa;

    public String getError() {
        return error;
    }

    public String getGlosa() {
        return glosa;
    }
}
