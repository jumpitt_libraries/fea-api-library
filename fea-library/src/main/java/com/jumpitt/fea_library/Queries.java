package com.jumpitt.fea_library;

import com.google.gson.Gson;
import com.jumpitt.fea_library.model.Auth;
import com.jumpitt.fea_library.model.BaseResponse;
import com.jumpitt.fea_library.model.Certificate;
import com.jumpitt.fea_library.model.CertificateStatus;
import com.jumpitt.fea_library.model.ErrorResponse;
import com.jumpitt.fea_library.model.RequestResponse;
import com.jumpitt.fea_library.model.SignResponse;
import com.jumpitt.fea_library.model.TransactionList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class Queries {
    private static final String SIGN = "0";
    private static final String REJECT = "1";

    public static void doAuth(String mobileID, final FEAListener<Auth> listener) {
        Call<Auth> call = RestClient.get().auth(new AuthBody(mobileID));
        call.enqueue(new Callback<Auth>() {
            @Override
            public void onResponse(Call<Auth> call, Response<Auth> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    Throwable cause = new Throwable(response.message());
                    listener.onError(new Throwable(String.valueOf(response.code()), cause));
                }
            }

            @Override
            public void onFailure(Call<Auth> call, Throwable t) {
                listener.onError(t);
            }
        });
    }

    public static void doCreation(String token, CertificateBody body, final FEAListener<Certificate> listener) {
        Call<Certificate> call = RestClient.get().certificate(token, body);
        call.enqueue(new Callback<Certificate>() {
            @Override
            public void onResponse(Call<Certificate> call, Response<Certificate> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    if (response.errorBody() != null) {
                        ErrorResponse errorBody = new Gson().fromJson(response.errorBody().charStream(), ErrorResponse.class);
                        Throwable cause = new Throwable(errorBody.getGlosa());
                        listener.onError(new Throwable(errorBody.getError(), cause));
                    } else {
                        Throwable cause = new Throwable(response.message());
                        listener.onError(new Throwable(String.valueOf(response.code()), cause));
                    }
                }
            }

            @Override
            public void onFailure(Call<Certificate> call, Throwable t) {
                listener.onError(t);
            }
        });
    }

    public static void getTransactions(String status, String token, FEAListener<TransactionList> listener) {
        getTransaction(status, token, null, listener);
    }

    public static void getTransaction(String status, String token, String transactionID, final FEAListener<TransactionList> listener) {
        Call<TransactionList> call = RestClient.get().getTransactions(token, new TransactionBody(transactionID, status));
        call.enqueue(new Callback<TransactionList>() {
            @Override
            public void onResponse(Call<TransactionList> call, Response<TransactionList> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    if (response.errorBody() != null) {
                        ErrorResponse errorBody = new Gson().fromJson(response.errorBody().charStream(), ErrorResponse.class);
                        Throwable cause = new Throwable(errorBody.getGlosa());
                        listener.onError(new Throwable(errorBody.getError(), cause));
                    } else {
                        Throwable cause = new Throwable(response.message());
                        listener.onError(new Throwable(String.valueOf(response.code()), cause));
                    }
                }
            }

            @Override
            public void onFailure(Call<TransactionList> call, Throwable t) {
                listener.onError(t);
            }
        });
    }

    public static void signDocument(String publicKey, String token, String transactionID, String password, final FEAListener<SignResponse> listener) {
        sign(publicKey, token, null, transactionID, password, SIGN, listener);
    }

    public static void rejectDocument(String publicKey, String token, String transactionID, String password, final FEAListener<SignResponse> listener) {
        sign(publicKey, token, null, transactionID, password, REJECT, listener);
    }

    public static void signDocument(String publicKey, String webSocketId, String token, String transactionID, String password, final FEAListener<SignResponse> listener) {
        sign(publicKey, token, webSocketId, transactionID, password, SIGN, listener);
    }

    public static void rejectDocument(String publicKey, String webSocketId, String token, String transactionID, String password, final FEAListener<SignResponse> listener) {
        sign(publicKey, token, webSocketId, transactionID, password, REJECT, listener);
    }


    private static void sign(String publicKey, String token, String webSocketId, String transactionID, String password, String signMethod, final FEAListener<SignResponse> listener) {
        Call<SignResponse> call = RestClient.get().sign(token, new SignBody(publicKey, transactionID, webSocketId, password, signMethod));
        call.enqueue(new Callback<SignResponse>() {
            @Override
            public void onResponse(Call<SignResponse> call, Response<SignResponse> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    if (response.errorBody() != null) {
                        ErrorResponse errorBody = new Gson().fromJson(response.errorBody().charStream(), ErrorResponse.class);
                        Throwable cause = new Throwable(errorBody.getGlosa());
                        listener.onError(new Throwable(errorBody.getError(), cause));
                    } else {
                        Throwable cause = new Throwable(response.message());
                        listener.onError(new Throwable(String.valueOf(response.code()), cause));
                    }
                }
            }

            @Override
            public void onFailure(Call<SignResponse> call, Throwable t) {
                listener.onError(t);
            }
        });
    }

    public static void signDEC(String publicKey, String token, String dni, String documentCode, String password, String signMethod, final FEAListener<SignResponse> listener) {
        Call<SignResponse> call = RestClient.get().signDEC(token, new SignDECBody(publicKey, dni, password, signMethod, documentCode));
        call.enqueue(new Callback<SignResponse>() {
            @Override
            public void onResponse(Call<SignResponse> call, Response<SignResponse> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    if (response.errorBody() != null) {
                        ErrorResponse errorBody = new Gson().fromJson(response.errorBody().charStream(), ErrorResponse.class);
                        Throwable cause = new Throwable(errorBody.getGlosa());
                        listener.onError(new Throwable(errorBody.getError(), cause));
                    } else {
                        Throwable cause = new Throwable(response.message());
                        listener.onError(new Throwable(String.valueOf(response.code()), cause));
                    }
                }
            }

            @Override
            public void onFailure(Call<SignResponse> call, Throwable t) {
                listener.onError(t);
            }
        });
    }

    public static void status(String token, final FEAListener<CertificateStatus> listener) {
        Call<CertificateStatus> call = RestClient.get().status(token);
        call.enqueue(new Callback<CertificateStatus>() {
            @Override
            public void onResponse(Call<CertificateStatus> call, Response<CertificateStatus> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    if (response.errorBody() != null) {
                        ErrorResponse errorBody = new Gson().fromJson(response.errorBody().charStream(), ErrorResponse.class);
                        Throwable cause = new Throwable(errorBody.getGlosa());
                        listener.onError(new Throwable(errorBody.getError(), cause));
                    } else {
                        Throwable cause = new Throwable(response.message());
                        listener.onError(new Throwable(String.valueOf(response.code()), cause));
                    }
                }
            }

            @Override
            public void onFailure(Call<CertificateStatus> call, Throwable t) {
                listener.onError(t);
            }
        });
    }

    public static void getRequests(String token, String dni, final FEAListener<Boolean> listener) {
        Call<RequestResponse> call = RestClient.get().requests(token, new StatusBody(dni));
        call.enqueue(new Callback<RequestResponse>() {
            @Override
            public void onResponse(Call<RequestResponse> call, Response<RequestResponse> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body().getRequestList() != null && !response.body().getRequestList().isEmpty());
                } else {
                    if (response.errorBody() != null) {
                        ErrorResponse errorBody = new Gson().fromJson(response.errorBody().charStream(), ErrorResponse.class);
                        Throwable cause = new Throwable(errorBody.getGlosa());
                        listener.onError(new Throwable(errorBody.getError(), cause));
                    } else {
                        Throwable cause = new Throwable(response.message());
                        listener.onError(new Throwable(String.valueOf(response.code()), cause));
                    }
                }
            }

            @Override
            public void onFailure(Call<RequestResponse> call, Throwable t) {
                listener.onError(t);
            }
        });
    }
}
