package com.jumpitt.fea_library;

import com.google.gson.annotations.SerializedName;

public class TransactionBody {
    @SerializedName("idTransaccion")
    private String transactionId;
    @SerializedName("estado")
    private String status;

    public TransactionBody(String transactionId, String status) {
        this.transactionId = transactionId;
        this.status = status;
    }
}
