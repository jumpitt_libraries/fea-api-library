package com.jumpitt.fea_library;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

class Utils {
    public static String encrypt(String pkString, String message) {
        String C_INSTANCE2 = "RSA/ECB/PKCS1Padding";
        try {
            PublicKey pk = getKey(pkString);
            Cipher cipher = Cipher.getInstance(C_INSTANCE2);
            cipher.init(Cipher.ENCRYPT_MODE, pk);
            byte[] encryptedByteData = cipher.doFinal(message.getBytes("UTF-8"));

            return byteToHex(encryptedByteData);
        } catch (NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static PublicKey getKey(String key) {
        try {
            byte[] byteKey = Base64.decode(key.getBytes(), Base64.DEFAULT);
            X509EncodedKeySpec X509publicKey = new X509EncodedKeySpec(byteKey);
            KeyFactory kf = KeyFactory.getInstance("RSA");

            return kf.generatePublic(X509publicKey);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private static String byteToHex(byte[] origin) {
        try {
            StringBuilder buffer = new StringBuilder();
            for (int i = 0; i < origin.length; i++) {
                buffer.append(Character.forDigit((origin[i] >> 4) & 0xF, 16));
                buffer.append(Character.forDigit((origin[i] & 0xF), 16));
            }
            return buffer.toString();
        } catch (Exception ignore) {
        }
        return null;
    }
}
