package com.jumpitt.fea_library;

import com.google.gson.annotations.SerializedName;

class SignBody {
    @SerializedName("idTransaccion")
    private String transactionId;
    @SerializedName("passwordEncriptado")
    private String cryptedPassword;
    @SerializedName("rechazar")
    private String reject;
    @SerializedName("sessionWebSocket")
    private String socketId;

    public SignBody(String publicKey, String transactionId, String socketId, String password, String reject) {
        String encryptedPassword = Utils.encrypt(publicKey, password);
        this.transactionId = transactionId;
        this.cryptedPassword = encryptedPassword;
        this.reject = reject;
        this.socketId = socketId == null ? "" : socketId;
    }
}
