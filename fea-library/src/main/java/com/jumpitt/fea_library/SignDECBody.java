package com.jumpitt.fea_library;

import com.google.gson.annotations.SerializedName;

class SignDECBody {
    @SerializedName("dni")
    private String dni;
    @SerializedName("passwordEncriptado")
    private String cryptedPassword;
    @SerializedName("rechazar")
    private String reject;
    @SerializedName("codigoDocumento")
    private String code;

    public SignDECBody(String publicKey, String dni, String password, String reject, String code) {
        String encryptedPassword = Utils.encrypt(publicKey, password);
        this.dni = dni;
        this.cryptedPassword = encryptedPassword;
        this.reject = reject;
        this.code = code;
    }
}
